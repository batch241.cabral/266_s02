import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        //First
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime number is: " + primeNumbers[0]);

        //Second
        ArrayList<String> friends = new ArrayList<>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("\nMy friends are: " + friends);

        //Third
        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("Toothpaste", 15);
        inventory.put("Toothbrush", 20);
        inventory.put("Soap", 12);

        System.out.println("\nOur current inventory consists of: " + inventory);

    }
}